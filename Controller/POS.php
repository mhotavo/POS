<?php

/**
 * This file is part of POS plugin for FacturaScripts
 * Copyright (C) 2022 Juan José Prieto Dzul <juanjoseprieto88@gmail.com>
 */

namespace FacturaScripts\Plugins\POS\Controller;

use FacturaScripts\Core\Base\Controller;
use FacturaScripts\Core\Base\ControllerPermissions;
use FacturaScripts\Dinamic\Model\User;
use FacturaScripts\Dinamic\Model\Producto;
use FacturaScripts\Plugins\POS\Lib\PointOfSaleCustomer;
use FacturaScripts\Plugins\POS\Lib\PointOfSaleOrder;
use FacturaScripts\Plugins\POS\Lib\PointOfSalePayments;
use FacturaScripts\Plugins\POS\Lib\PointOfSaleProduct;
use FacturaScripts\Plugins\POS\Lib\PointOfSaleRequest;
use FacturaScripts\Plugins\POS\Lib\PointOfSaleSession;
use FacturaScripts\Plugins\POS\Lib\PointOfSaleTrait;
use FacturaScripts\Plugins\POS\Model\MovimientoPuntoVenta;
use Symfony\Component\HttpFoundation\Response;
use FacturaScripts\Core\Base\DataBase;

class POS extends Controller
{
    use PointOfSaleTrait;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var PointOfSaleOrder
     */
    protected $lastOrder;

    /**
     * @param Response $response
     * @param User $user
     * @param ControllerPermissions $permissions
     */
    public function privateCore(&$response, $user, $permissions)
    {
        parent::privateCore($response, $user, $permissions);
        $this->setTemplate(false);

        $this->session = new PointOfSaleSession($user);
        $action = $this->request->request->get('action', '');

        if ($action && false === $this->execAction($action)) {
            return;
        }

        $this->execAfterAction($action);

        $template = $this->session->getView();
        $this->setTemplate($template);
    }

    protected function execAction(string $action): bool
    {
        switch ($action) {
            case 'search-barcode':
                $this->searchBarcode();
                return false;

            case 'search-customer':
                $this->searchCustomer();
                return false;

            case 'search-product':
                $this->searchProduct();
                return false;

            case 'cash-movment':
                $this->saveMovments();
                return true;

            case 'get-product-stock':
                $this->searchStock();
                return false;

            case 'recalculate-order':
                $this->recalculateOrder();
                return false;

            case 'save-new-customer':
                $this->saveNewCustomer();
                return false;

            case 'hold-order':
                $this->saveOrderOnHold();
                return false;

            case 'save-order':
                $this->saveOrder();
                return false;

            case 'get-orders-on-hold':
                $this->setResponse($this->getStorage()->getOrdersOnHold());
                return false;

            case 'get-last-orders':
                $this->setResponse($this->getStorage()->getLastOrders());
                return false;

            case 'delete-order-on-hold':
                $this->deleteOrderOnHold();
                return false;

            case 'resume-order':
                $this->resumeOrder();
                return false;

            case 'reprint-order':
                $this->reprintOrder();
                return false;
            case 'print-closing-voucher':
                $this->printClosingVoucher();
                $this->buildResponse();
                return false;

            default:
                $this->setResponse('Funcion no encontrada');
                return true;
        }
    }

    protected function execAfterAction(string $action)
    {
        switch ($action) {
            case 'open-session':
                $this->initSession();
                break;
            case 'open-terminal':
                $this->loadTerminal();
                break;
            case 'close-session':
                $this->closeSession();
                break;
        }
    }

    /**
     * Search product by barcode.
     */
    protected function searchBarcode()
    {
        $producto = new PointOfSaleProduct();
        $barcode = $this->request->request->get('query');

        $this->setResponse($producto->searchBarcode($barcode));
    }

    /**
     * Search customer by text.
     */
    protected function searchCustomer()
    {
        $customer = new PointOfSaleCustomer();
        $query = $this->request->request->get('query');

        $this->setResponse($customer->search($query));
    }

    /**
     * Search product by text.
     */
    protected function searchProduct()
    {
        $product = new PointOfSaleProduct();
        $query = $this->request->request->get('query', '');

        $source = $this->getTerminal()->productsource;
        $company = '';
        $warehouse = '';

        if ($source) {
            switch ($source) {
                case 1:
                    $company = $this->getTerminal()->idempresa;
                    break;
                case 2:
                    $warehouse = $this->getTerminal()->codalmacen;
            }
        }

        $this->setResponse($product->search($query, [], $warehouse, $company));
    }

    /**
     * Search product by text.
     */
    protected function searchStock()
    {
        $product = new PointOfSaleProduct();
        $query = $this->request->request->get('query', '');

        $this->setResponse($product->getStock($query));
    }

    /**
     * @param array $data
     * @return void
     */
    protected function buildResponse(array $data = [])
    {
        $response = $data;
        $response['messages'] = $this->getMessages();
        $response['token'] = $this->token;

        $this->setResponse($response);
    }

    protected function initSession()
    {
        $terminal = $this->request->request->get('terminal', '');
        $amount = $this->request->request->get('saldoinicial', 0) ?: 0;
        $this->session->openSession($terminal, $amount);
    }

    /**
     * Close current user POS session.
     */
    protected function closeSession()
    {
        $cash = $this->request->request->get('cash');
        $this->session->closeSession($cash);

        $this->printClosingVoucher();
    }

    /**
     * Set a held order as complete to remove from list.
     */
    protected function deleteOrderOnHold()
    {
        $code = $this->request->request->get('code', '');

        if ($this->getStorage()->updateOrderOnHold($code)) {
            $this->toolBox()->i18nLog()->info('pos-order-on-hold-deleted');
        }

        $this->setNewToken();
        $this->buildResponse();
    }

    public static function round_money($value)
    {
        if ($value > 1000) {
            $decenas = substr($value, -2); // two last numbers
            $decenas = intval($decenas);
            if ($decenas > 0 && $decenas <= 50) {
                $faltante = intval(50 - $decenas);
                $value = intval($value);
                $value += $faltante;
            } else if ($decenas > 50 && $decenas <= 99) {
                $faltante = intval(100 - $decenas);
                $value = intval($value);
                $value += $faltante;
            }
        }
        return $value;
    }

    /**
     * Recalculate order data.
     *
     * @return void
     */
    protected function recalculateOrder()
    {
        $request = new PointOfSaleRequest($this->request);
        $order = new PointOfSaleOrder($request);

        $data = $order->recalculate();

        $sum_pvpsindto = 0;
        $sum_neto = 0;
        foreach ($data['lines'] as $key => $value) {
            $dataBase = new DataBase();
            $sum_pvpsindto += $value->pvpsindto;
            $sum_neto += $value->pvptotal;
            $sql = 'SELECT sell_by_weight from productos where idproducto = ' . $value->idproducto;
            $sell_by_weight = $dataBase->select($sql)[0]['sell_by_weight'];

            if ($sell_by_weight == 1) {
                //  Initiate curl
                $url = $this->toolBox()->appSettings()->get('pos', 'url');

                if ($url == "") {
                    $sql = 'SELECT COALESCE(weight, 0) AS weight
                    from peso_balanza where creationdate> NOW() - INTERVAL 3 SECOND
                    ORDER BY id DESC LIMIT 1';
                    $result = $dataBase->select($sql);

                    if (count($result) > 0) {
                        $grams = $result[0]['weight'];
                        $value->cantidad = intval($grams);
                        $value->pvpsindto  =  $this->round_money(intval($grams) * $value->pvpunitario);
                    } else {
                        $value->pvpsindto  =  $this->round_money(intval($value->pvpsindto));
                        $value->pvptotal  =  $this->round_money(intval($value->pvptotal));
                    }

                    if ($value->pvpsindto < 4000) {
                        $value->pvpsindto = 4000;
                    }

                    $value->pvptotal = $value->pvpsindto;
                } else {
                    $ch = \curl_init();
                    // Will return the response, if false it print the response
                    // \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    // Set the url
                    \curl_setopt($ch, CURLOPT_URL, $url);
                    \curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    \curl_setopt($ch, CURLOPT_SSLVERSION, 1);
                    \curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
                    \curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    \curl_setopt($ch, CURLOPT_PROXY, $url);
                    // Execute
                    $result = \curl_exec($ch);

                    if ($result) {
                        // Will dump a beauty json :3
                        $obj = json_decode($result, true);
                        $value->cantidad = intval($obj["grams"]);
                        $value->pvpsindto  =  $this->round_money(intval($obj["grams"]) * $value->pvpunitario);
                        $value->pvptotal = $value->pvpsindto;
                    } else {
                        $value->pvpsindto  =  $this->round_money(intval($value->pvpsindto));
                        $value->pvptotal  =  $this->round_money(intval($value->pvptotal));
                    }
                    // Closing
                    \curl_close($ch);
                }
            }
        }

        $data['doc']->neto = $sum_neto;
        $data['doc']->netosindto = $sum_pvpsindto;
        $this->setResponse($data);
    }

    /**
     * Load order on hold by code.
     */
    protected function resumeOrder()
    {
        $code = $this->request->request->get('code', '');

        $this->setNewToken();
        $this->buildResponse($this->getStorage()->getOrderOnHold($code));
    }

    /**
     * Reprint order by code.
     */
    protected function reprintOrder()
    {
        $code = $this->request->request->get('code', '');

        if ($code) {
            $order = $this->getStorage()->getOrder($code);
            $this->printVoucher($order->getDocument());
            $this->buildResponse();
        }
    }

    protected function saveNewCustomer()
    {
        $customer = new PointOfSaleCustomer();

        $taxID = $this->request->request->get('taxID');
        $name = $this->request->request->get('name');

        if ($customer->saveNew($taxID, $name)) {
            $this->setResponse($customer->getCustomer());
        }

        $this->buildResponse();
    }

    protected function saveMovments()
    {
        if (false === $this->validateRequest()) return;

        $amount = $this->request->request->get('amount');
        $description = $this->request->request->get('description');

        $movment = new MovimientoPuntoVenta();
        $movment->idsesion = $this->getSession()->getSession()->idsesion;
        $movment->nickusuario = $this->user->nick;
        $movment->descripcion = $description;
        $movment->total = $amount ?? 0;

        if ($movment->save()) {
            $this->session->updateCashAmount($movment->total);
            self::toolBox()::log()->info('Movimiento cuardado correctamente.');
        }

        $this->buildResponse();
    }

    /**
     * Save order and payments.
     *
     * @return void
     */
    protected function saveOrder(): void
    {
        if (false === $this->validateRequest()) return;

        $orderRequest = new PointOfSaleRequest($this->request);
        $order = new PointOfSaleOrder($orderRequest);
        $this->dataBase->beginTransaction();

        if (false === $this->getStorage()->saveOrder($order)) {
            $this->dataBase->rollback();
            $this->buildResponse();
            return;
        }

        $this->dataBase->commit();

        $this->savePayments($order->getPayments());
        $this->printVoucher($order->getDocument());
        $this->lastOrder = $order;
        $this->buildResponse();
    }

    /**
     * Put order on hold.
     *
     * @return void
     */
    protected function saveOrderOnHold(): void
    {
        if (false === $this->validateRequest()) return;

        $request = new PointOfSaleRequest($this->request, true);
        $order = new PointOfSaleOrder($request);

        $this->dataBase->beginTransaction();

        if (false === $order->save()) {
            $this->toolBox()->i18nLog()->warning('pos-order-on-hold-error');
            $this->dataBase->rollback();
            return;
        }

        $this->dataBase->commit();

        $this->toolBox()->i18nLog()->info('pos-order-on-hold');
        $this->buildResponse();
    }

    protected function savePayments(array $payments)
    {
        $order = $this->getStorage()->getCurrentOrder();

        $storage = new PointOfSalePayments($order, $this->getCashPaymentMethod());
        $storage->savePayments($payments);

        $this->session->updateCashAmount($storage->getCashPaymentAmount());
    }

    /**
     * @return void
     */
    protected function loadTerminal()
    {
        $id = $this->request->request->get('terminal', '');
        $this->getSession()->getTerminal($id);
    }

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData(): array
    {
        $pagedata = parent::getPageData();
        $pagedata['title'] = 'POS';
        $pagedata['menu'] = 'point-of-sale';
        $pagedata['icon'] = 'fas fa-shopping-cart';
        $pagedata['showonmenu'] = true;

        return $pagedata;
    }
}
